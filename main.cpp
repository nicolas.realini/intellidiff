#include <iostream>
#include <fstream>
#include <cfloat>
#include <cmath>
#include <set>


int main(int argc, char *argv[]) {
    if (argc < 5) {
        std::cerr << "Usage: intellidiff <file1> <file2> <MAX_ABS_ERR> <MAX_REL_ERR>" << std::endl;
        return 1;
    }

    const double MAX_ABS_ERR = std::strtod(argv[3], nullptr);
    const double MAX_REL_ERR = std::strtod(argv[4], nullptr);

    std::ifstream file1, file2;
    file1.open(argv[1]);
    file2.open(argv[2]);

    if (!file1.is_open() || !file2.is_open()) {
        std::cerr << "Error opening files" << std::endl;
        return 1;
    }
    std::printf("Testing file %s for differences...\n", argv[1]);

    std::string word1, word2;
    double val1, val2, delta_max = 0, delta_min = DBL_MAX;
    int delta_cnt = 0, unacceptable_cnt = 0;
    long double delta_avg = 0;
    std::set<double> deltas;
    while (file1 >> word1 && file2 >> word2) {
        try { // Try to convert to double
            val1 = std::stod(word1);
            val2 = std::stod(word2);
            double rel_err = 0, abs_err = std::abs(val1) - std::abs(val2);
            if (val1 == val2) continue;
            delta_cnt++;
            if (val1 < val2) {
                rel_err = std::abs(((val2 - val1) * 100) / val2);
            } else {
                rel_err = std::abs(((val1 - val2) * 100) / val1);
            }
            deltas.insert(rel_err);
            if (rel_err < delta_min) delta_min = rel_err;
            if (rel_err > delta_max) delta_max = rel_err;

            if (rel_err > MAX_REL_ERR && abs_err > MAX_ABS_ERR) {
                std::printf("%f %f rel err: %f abs err: %f\n", val1, val2, rel_err, abs_err);
                unacceptable_cnt++;
            }
        } catch (std::invalid_argument&) { // not values, check as strings
            if (word1 != word2) {
                std::cout << word1 << " != " << word2 << std::endl;
            }
        }
    }

    std::cout << "--------------------" << std::endl;
    std::cout << "Results: " << std::endl;

    std::printf("%d differences found\n", delta_cnt);
    if (delta_cnt == 0) return 0;

    auto it = deltas.begin();
    std::advance(it, deltas.size() / 2);
    std::printf("Mean delta: %f\n", *it);

    for(auto const& val : deltas) {
        delta_avg += val;
    }
    std::printf("Average delta: %Lf\n", delta_avg / deltas.size());

    std::cout << "Worst: " << *--deltas.end() << std::endl;
    std::cout << "Best: " << *deltas.begin() << std::endl;
    std::cout << "Unacceptable values: " << unacceptable_cnt << std::endl << std::endl;
    return 0;
}
